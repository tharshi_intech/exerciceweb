"use trict";
const fs = require ("fs");
require ("remedial");

const choisir = function (req, res, query) {
let secret;
let page;
let lire;

//Generer un nombre aleatoire
secret = Math.floor(Math.random() * 10) + 1;
secret = Number(secret);

//Cree un fichier secret
lire = fs.writeFileSync("secret", secret, "UTF-8");
console.log(" secret : " + secret);

// affichage de la page modele commencer
page = fs.readFileSync("modele_choisir.html", "UTF-8");

let marqueurs = {};
marqueurs.variable = "choissis un nombre entre 1 et 10";
page = page.supplant(marqueurs);


res.writeHead(200, {"Content-Type": "text/html"});
res.write(page);
res.end();

};

module.exports = choisir;

